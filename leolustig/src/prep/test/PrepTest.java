package prep.test;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import prep.Prep;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

class PrepTest {

    private Prep prep = null;

    @BeforeEach
    void setUp() {
        prep = new Prep();
    }

    @AfterEach
    void tearDown() {
        prep = null;
    }

    @Test
    void countDuplicates() {
        assertEquals(0, prep.countDuplicates(new int[]{0, 1, 2}));
        assertEquals(0, prep.countDuplicates(new int[]{0, 1, 2, 3}));
        assertEquals(1, prep.countDuplicates(new int[]{0, 1, 2, 3, 3}));
    }

    @Test
    void countingValleys() {
        assertEquals(1, prep.countingValleys("DUUD"));
        assertEquals(3, prep.countingValleys("DUUDDUDU"));
        assertEquals(4, prep.countingValleys("DUDUDUDU"));
        assertEquals(0, prep.countingValleys("UDUDUDUD"));
        assertEquals(4, prep.countingValleys("DUUDDUUDDUUDDUUD"));
        assertEquals(1, prep.countingValleys("DU"));
    }

    @Test
    void jumpingOnClouds() {
        assertEquals(4, prep.jumpingOnClouds(new int[]{0, 0, 1, 0, 0, 1, 0}));
        assertEquals(3, prep.jumpingOnClouds(new int[]{0, 0, 0, 0, 1, 0}));
        assertEquals(8, prep.jumpingOnClouds(new int[]{0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0}));
        assertEquals(16, prep.jumpingOnClouds(new int[]{0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0}));
    }

    @Test
    void repeatedString() {
        assertEquals(10L, prep.repeatedString("a", 10L));
        assertEquals(7L, prep.repeatedString("aba", 10L));
        assertEquals(1000000000000L, prep.repeatedString("a", 1000000000000L));
        assertEquals(500000000000L, prep.repeatedString("ab", 1000000000000L));
        assertEquals(2000L, prep.repeatedString("abcde", 9999L));
        assertEquals(588525L, prep.repeatedString("aab", 882787L));
        assertEquals(0L, prep.repeatedString("hello", 3L));
        assertEquals(1L, prep.repeatedString("hallo", 3L));
        assertEquals(51574523448L, prep.repeatedString("kmretasscityylpdhuwjirnqimlkcgxubxmsxpypgzxtenweirknjtasxtvxemtwxuarabssvqdnktqadhyktagjxoanknhgilnm", 736778906400L));
    }

    @Test
    void getMoneySpent() {
        assertEquals(9, prep.getMoneySpent(new int[]{3,1}, new int[]{5,2,8}, 10));
        assertEquals(5, prep.getMoneySpent(new int[]{3}, new int[]{2,3}, 5));
        assertEquals(-1, prep.getMoneySpent(new int[]{4}, new int[]{5}, 5));
        assertEquals(10, prep.getMoneySpent(new int[]{3,3,3,3}, new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9}, 10));
    }

    @Test
    void compareTripletsMyVersion() {
        assertArrayEquals(new int[]{1,1}, prep.compareTripletsMyVersion(new int[]{5,6,7}, new int[]{3,6,10}));
        assertArrayEquals(new int[]{2,1}, prep.compareTripletsMyVersion(new int[]{17, 28, 30}, new int[]{99, 16, 8}));
    }

    @Test
    void compareTriplets() {
        List<Integer> result = new ArrayList<>();
        result.add(1);
        result.add(1);

        List<Integer> a = new ArrayList<>();
        a.add(5);
        a.add(6);
        a.add(7);

        List<Integer> b = new ArrayList<>();
        b.add(3);
        b.add(6);
        b.add(10);

        List<Integer> returnValue = prep.compareTriplets(a, b);
        for (int i = 0; i < result.size(); i++) {
            assertEquals(result.get(i), returnValue.get(i));
        }

        List<Integer> result1 = new ArrayList<>();
        result1.add(2);
        result1.add(1);

        List<Integer> a1 = new ArrayList<>();
        a1.add(17);
        a1.add(18);
        a1.add(30);

        List<Integer> b1 = new ArrayList<>();
        b1.add(99);
        b1.add(16);
        b1.add(8);

        List<Integer> returnValue1 = prep.compareTriplets(a1, b1);
        for (int i = 0; i < result1.size(); i++) {
            assertEquals(result1.get(i), returnValue1.get(i));
        }
    }

    @Test
    void hourglassSum() {
        assertEquals(19, prep.hourglassSum(new int[][]{
                {1, 1, 1, 0, 0, 0},
                {0, 1, 0, 0, 0, 0},
                {1, 1, 1, 0, 0, 0},
                {0, 0, 2, 4, 4, 0},
                {0, 0, 0, 2, 0, 0},
                {0, 0, 1, 2, 4, 0}
        }));
    }
}