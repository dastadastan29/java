package prep;

import java.util.*;
import java.util.function.BiFunction;

public class Prep {

    public int countDuplicates(int[] arr) {
        Map<Integer, Boolean> hashMap = new HashMap<>();
        int count = 0;
        for(int x : arr) {
            if (!hashMap.containsKey(x)) {
                hashMap.put(x, true);
                continue;
            }
            count++;
        }

        return count;
    }

    public int countingValleys(String s) {
        int acc = 0;
        int leftTheValley = 0;
        StringBuilder sbU = new StringBuilder();
        StringBuilder sbD = new StringBuilder();

        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == 'D') {
                acc--;
                if (acc < 0) {
                    sbD.append("\\");
                    sbU.append(" ");
                } else {
                    sbD.append(" ");
                    sbU.append("\\");
                }
            } else {
                if (acc < 0) {
                    sbD.append("/");
                    sbU.append(" ");
                } else {
                    sbD.append(" ");
                    sbU.append("/");
                }
                acc++;
                if (acc == 0) {
                    leftTheValley++;
                }
            }
        }
        Character c = s.charAt(0);

        System.out.println(sbU.toString());
        System.out.println(sbD.toString());

        return leftTheValley;
    }

    public int jumpingOnClouds(int[] ints) {
        return (int)jumps(0, ints, ints.length);
    }

    private long jumps(int currentPos, int[] arr, int length) {
        if (currentPos == length - 1) return 0;
        if (currentPos + 2 < length && arr[currentPos+2] == 0) {
            currentPos += 2;
        } else if (currentPos + 1 < length) {
            currentPos++;
        }
        return 1 + jumps(currentPos, arr, length);
    }

    public long repeatedString(String s, long l) {
        long length = s.length();
        ArrayList<Integer> arr = new ArrayList<>(s.length());
        int count = 0;
        int i = 0;

        while (i < s.length()) {
            if (s.charAt(i) == 'a') count++;
            arr.add(i, count);
            i++;
        }
        long fullLength = l / length;
        long fracLength = l % length;
        long total;

        if (fracLength > 0) {
            total = (long)arr.get((int)length - 1) * fullLength + (long)arr.get((int)fracLength - 1);
        } else {
            total = (long)arr.get((int)length - 1) * fullLength;
        }

        return total;
    }

    public int getMoneySpent(int[] keyboards, int[] drives, int budget) {
        int acc = -1;
        int temp;
        for(int d : drives) {
            for(int k : keyboards) {
                temp = d + k;
                if (temp > budget) continue;
                if (temp > acc) acc = temp;
            }
        }
        return acc;
    }

    public int[] compareTripletsMyVersion(int[] tripletA, int[] tripletB) {
        int[] result = new int[2];

        for(int i = 0; i < tripletA.length; i++) {
            if (tripletA[i] > tripletB[i]) {
                result[0]++;
            } else if (tripletA[i] < tripletB[i]) {
                result[1]++;
            }
        }
        return result;
    }

    public List<Integer> compareTriplets(List<Integer> a, List<Integer> b) {
        List<Integer> result = new ArrayList<>();
        result.add(0);
        result.add(0);

        for (int i = 0; i < a.size(); i++) {
            if (a.get(i) > b.get(i)) {
                result.set(0, result.get(0) + 1);
            } else if (a.get(i) < b.get(i)) {
                result.set(1, result.get(1) + 1);
            }
        }
        return result;
    }

    public int hourglassSum(int[][] arr) {
        List<Integer> sumList = new ArrayList<>();
        BiFunction<Integer, Integer, Integer> add = (x, y) -> arr[x][y] + arr[x][y+1] + arr[x][y+2] +
                arr[x+1][y+1] + arr[x+2][y] + arr[x+2][y+1] + arr[x+2][y+2];
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                sumList.add(add.apply(i, j));
            }
        }
        return Collections.max(sumList);
    }
}

